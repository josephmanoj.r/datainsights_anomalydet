import sys 
sys.path.insert(0, './src/main/')
  
from model import read_data, process_data
import pytest
import pandas as pd

class TestModel:
    def test_data_empty(self):
        global df_test
        df_test=read_data('2021-04-01', '2021-06-30')
        assert len(df_test)==len(read_data('2021-04-01', '2021-06-30'))
